package com.dolgalyova.qr_scan.common.data.database

import androidx.room.*
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity

@Dao
interface ScanDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(scan: ScanEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(scan: ScanEntity)

    @Delete
    fun delete(scan: ScanEntity)

    @Query("SELECT * FROM scan")
    fun getScans(): List<ScanEntity>

    @Query("SELECT * FROM scan WHERE fiscalSign = :fiscalSign")
    fun getScanByFiscalSign(fiscalSign: Long): ScanEntity
}