package com.dolgalyova.qr_scan.common.arch

interface OnBackPressListener {
    fun onBackPress(): Boolean
}