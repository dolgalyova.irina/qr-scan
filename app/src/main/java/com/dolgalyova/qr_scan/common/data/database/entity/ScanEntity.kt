package com.dolgalyova.qr_scan.common.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "scan")
data class ScanEntity(@PrimaryKey val fiscalSign: Long,
                      val fiscalNumber: Long,
                      val fiscalDocument: Long,
                      val date: String,
                      val sum: Int,
                      val isSent: Boolean)