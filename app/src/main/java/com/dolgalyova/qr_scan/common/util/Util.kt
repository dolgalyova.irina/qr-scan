package com.dolgalyova.qr_scan.common.util

import com.dolgalyova.qr_scan.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

inline fun <T> getOrNull(body: () -> T?) = try {
    body()
} catch (e: Exception) {
    null
}

fun <T> createApiService(clazz: Class<T>, client: OkHttpClient): T {
    return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(clazz)
}