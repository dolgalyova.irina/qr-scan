package com.dolgalyova.qr_scan.common.util

import android.view.View
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.qr_scan.common.view.LoadMoreScrollListener

fun RecyclerView.setLoadMoreListener(onLoadNext: () -> Unit) {
    this.addOnScrollListener(LoadMoreScrollListener(onLoadNext))
}

fun View.getString(@StringRes idRes: Int, vararg formatArgs: Any) = resources.getString(idRes, formatArgs)