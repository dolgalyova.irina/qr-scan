package com.dolgalyova.qr_scan.common.arch

import io.reactivex.Scheduler

data class RxWorkers(
        val subscribeWorker: Scheduler,
        val observeWorker: Scheduler
)