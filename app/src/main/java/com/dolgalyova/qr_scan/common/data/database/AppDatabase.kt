package com.dolgalyova.qr_scan.common.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity

@Database(entities = [ScanEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val scan: ScanDao
}