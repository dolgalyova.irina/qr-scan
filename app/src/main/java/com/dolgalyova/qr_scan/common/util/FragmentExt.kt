package com.dolgalyova.qr_scan.common.util

import android.Manifest
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.annotation.IntegerRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import com.dolgalyova.qr_scan.R

fun FragmentActivity.canHandleNavigation() = this.lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)

fun Fragment.canHandleNavigation() = this.lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)

fun FragmentManager.clearBackStack() = repeat((0..this.backStackEntryCount).count()) { this.popBackStack() }

fun Fragment.getInteger(@IntegerRes idRes: Int) = resources.getInteger(idRes)

fun Fragment.showGeneralError() {
    context?.let { Toast.makeText(it, R.string.error_general, Toast.LENGTH_SHORT).show() }
}

fun Fragment.hasCameraPermission(): Boolean {
    return context?.let {
        ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
    } ?: false
}