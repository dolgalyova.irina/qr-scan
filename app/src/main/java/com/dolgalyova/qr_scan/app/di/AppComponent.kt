package com.dolgalyova.qr_scan.app.di

import com.dolgalyova.qr_scan.app.App
import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.data.database.AppDatabase
import com.dolgalyova.qr_scan.screen.host.router.NavigationHost
import dagger.BindsInstance
import dagger.Component
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(target: App)

    fun provideNavigationHost(): NavigationHost
    fun provideOkHttpClient(): OkHttpClient
    fun provideDatabase(): AppDatabase
    fun provideWorkers(): RxWorkers

    @Component.Builder
    interface Builder {
        fun module(module: AppModule): Builder
        @BindsInstance
        fun target(target: App): Builder

        fun build(): AppComponent
    }
}