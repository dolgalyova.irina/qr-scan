package com.dolgalyova.qr_scan.app

import android.app.Application
import com.dolgalyova.qr_scan.BuildConfig
import com.dolgalyova.qr_scan.app.di.AppModule
import com.dolgalyova.qr_scan.app.di.DaggerAppComponent
import timber.log.Timber

class App : Application() {
    val component by lazy {
        DaggerAppComponent.builder()
                .module(AppModule())
                .target(this)
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}