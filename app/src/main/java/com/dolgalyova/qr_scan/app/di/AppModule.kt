package com.dolgalyova.qr_scan.app.di

import android.content.Context
import androidx.room.Room
import com.dolgalyova.qr_scan.app.App
import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.data.database.AppDatabase
import com.dolgalyova.qr_scan.screen.host.router.AppNavigationHost
import com.dolgalyova.qr_scan.screen.host.router.NavigationHost
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun appContext(application: App): Context = application.applicationContext

    @Provides
    @Singleton
    fun navigationHost(): NavigationHost = AppNavigationHost()

    @Provides
    @Singleton
    fun client(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
    }

    @Provides
    @Singleton
    fun scanDB(application: App): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "db-scans")
                .allowMainThreadQueries()
                .build()
    }

    @Provides
    @Singleton
    fun workers() = RxWorkers(Schedulers.io(), AndroidSchedulers.mainThread())
}