package com.dolgalyova.qr_scan.screen.main_flow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.qr_scan.R
import com.dolgalyova.qr_scan.common.arch.OnBackPressListener
import com.dolgalyova.qr_scan.screen.main_flow.di.MainFlowComponent
import com.dolgalyova.qr_scan.screen.make_scan.di.MakeScanComponent
import com.dolgalyova.qr_scan.screen.make_scan.di.MakeScanModule
import com.dolgalyova.qr_scan.screen.scan_list.di.ScanListComponent
import com.dolgalyova.qr_scan.screen.scan_list.di.ScanListModule
import javax.inject.Inject

class MainFlowFragment : Fragment(), ScanListComponent.ComponentProvider,
        MakeScanComponent.ComponentProvider, OnBackPressListener {

    companion object {
        fun create() = MainFlowFragment()
    }

    private val component by lazy {
        (activity as MainFlowComponent.ComponentProvider).provideMainFlowComponent(this)
    }

    @Inject lateinit var viewModelFactory: MainFlowViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_flow_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewModelProviders.of(this, viewModelFactory).get(MainFlowViewModel::class.java)
    }

    override fun provideScanListComponent(): ScanListComponent {
        return component.plusScanList()
                .module(ScanListModule())
                .build()
    }

    override fun provideMakeScanComponent(): MakeScanComponent {
        return component.plusMakeScan()
                .module(MakeScanModule())
                .build()
    }

    override fun onBackPress(): Boolean {
        val topFragment = childFragmentManager.fragments.firstOrNull { it is OnBackPressListener }
        val processed = if (topFragment?.isVisible == true) {
            (topFragment as? OnBackPressListener)?.onBackPress() ?: false
        } else false
        return processed
    }
}