package com.dolgalyova.qr_scan.screen.scan_details.domain

import android.net.Uri
import com.dolgalyova.qr_scan.BuildConfig
import com.dolgalyova.qr_scan.common.arch.Interactor
import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.util.composeWith
import com.dolgalyova.qr_scan.screen.scan_details.data.ScanDetailsRepository

class ScanDetailsInteractor(private val repository: ScanDetailsRepository,
                            private val scanId: ScanId,
                            private val workers: RxWorkers) : Interactor() {
    private companion object {
        const val URL_PATH = "${BuildConfig.API_URL}check"
        const val QUERY_FISCAL_NUMBER = "fiscalNumber"
        const val QUERY_FISCAL_SIGN = "fiscalSign"
        const val QUERY_FISCAL_DOCUMENT = "fiscalDocument"
        const val QUERY_DATE = "date"
        const val QUERY_SUM = "sum"
        const val QUERY_USER = "user"
        const val FAKE_USER_ID = "1"
    }

    fun getScan(onSuccess: (String) -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.getScan(scanId.id)
                .map {
                    Uri.Builder()
                            .encodedPath(URL_PATH)
                            .appendQueryParameter(QUERY_FISCAL_NUMBER, it.fiscalNumber.toString())
                            .appendQueryParameter(QUERY_FISCAL_SIGN, it.fiscalSign.toString())
                            .appendQueryParameter(QUERY_FISCAL_DOCUMENT, it.fiscalDocument.toString())
                            .appendQueryParameter(QUERY_DATE, it.date)
                            .appendQueryParameter(QUERY_SUM, it.sum.toString())
                            .appendQueryParameter(QUERY_USER, FAKE_USER_ID)
                            .build()
                            .toString()
                }
                .composeWith(workers)
                .subscribe(onSuccess, onError))
    }
}

inline class ScanId(val id: Long)