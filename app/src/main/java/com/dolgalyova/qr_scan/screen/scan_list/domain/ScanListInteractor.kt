package com.dolgalyova.qr_scan.screen.scan_list.domain

import com.dolgalyova.qr_scan.common.arch.Interactor
import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import com.dolgalyova.qr_scan.common.util.composeWith
import com.dolgalyova.qr_scan.screen.scan_list.data.ScanListRepository

class ScanListInteractor(private val repository: ScanListRepository,
                         private val workers: RxWorkers) : Interactor() {

    fun getScans(page: Int, onSuccess: (List<ScanEntity>) -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.getScans(page)
                .composeWith(workers)
                .subscribe(onSuccess, onError)
        )
    }
}