package com.dolgalyova.qr_scan.screen.scan_details.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.qr_scan.screen.scan_details.domain.ScanDetailsInteractor
import com.dolgalyova.qr_scan.screen.scan_details.router.ScanDetailsRouter

class ScanDetailsViewModelFactory(private val interactor: ScanDetailsInteractor,
                                  private val router: ScanDetailsRouter) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = ScanDetailsViewModel(interactor, router) as T
}