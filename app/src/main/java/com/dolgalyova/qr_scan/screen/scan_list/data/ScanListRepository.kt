package com.dolgalyova.qr_scan.screen.scan_list.data

import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import io.reactivex.Single

interface ScanListRepository {

    fun getScans(page: Int): Single<List<ScanEntity>>
}