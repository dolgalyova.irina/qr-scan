package com.dolgalyova.qr_scan.screen.main_flow.domain

sealed class MainFlowInitialScreen {
    object Recent : MainFlowInitialScreen()
}