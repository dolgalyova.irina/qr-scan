package com.dolgalyova.qr_scan.screen.main_flow.router

import com.dolgalyova.qr_scan.screen.make_scan.router.MakeScanRouter

class MakeScanFragmentRouter(private val navigator: MainFlowNavigator) : MakeScanRouter {

    override fun exit() = navigator.setScreen(MainFlowNavigator.Screen.RecentScans)
}