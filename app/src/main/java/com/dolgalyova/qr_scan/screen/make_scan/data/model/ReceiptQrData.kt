package com.dolgalyova.qr_scan.screen.make_scan.data.model

data class ReceiptQrData(val fiscalNumber: Long,
                         val fiscalSign: Long,
                         val fiscalDocument: Long,
                         val date: String,
                         val sum: Int)