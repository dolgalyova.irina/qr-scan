package com.dolgalyova.qr_scan.screen.make_scan.source

import io.reactivex.Completable
import retrofit2.http.GET
import retrofit2.http.Query

interface ScanRemoteSource {

    @GET("check")
    fun getScanInfo(@Query("fiscalNumber") fiscalNumber: Long,
                    @Query("fiscalSign") fiscalSign: Long,
                    @Query("fiscalDocument") fiscalDocument: Long,
                    @Query("date") date: String,
                    @Query("sum") sum: Int): Completable
}