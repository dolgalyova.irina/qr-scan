package com.dolgalyova.qr_scan.screen.make_scan.data

import io.reactivex.Completable

interface MakeScanRepository {

    fun saveResult(result: String): Completable
}