package com.dolgalyova.qr_scan.screen.host.di

import com.dolgalyova.qr_scan.app.di.AppComponent
import com.dolgalyova.qr_scan.common.di.ActivityScope
import com.dolgalyova.qr_scan.screen.host.MainActivity
import com.dolgalyova.qr_scan.screen.main_flow.di.MainFlowComponent
import com.dolgalyova.qr_scan.screen.scan_details.di.ScanDetailsComponent
import dagger.BindsInstance
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [MainModule::class, MainNavigationModule::class])
interface MainComponent {

    fun inject(target: MainActivity)

    fun plusMainFlow(): MainFlowComponent.Builder
    fun plusScanDetailsFlow(): ScanDetailsComponent.Builder

    @Component.Builder
    interface Builder {
        fun parent(parent: AppComponent): Builder
        fun module(module: MainModule): Builder
        fun navigationModule(navigationModule: MainNavigationModule): Builder
        @BindsInstance fun target(target: MainActivity): Builder

        fun build(): MainComponent
    }
}