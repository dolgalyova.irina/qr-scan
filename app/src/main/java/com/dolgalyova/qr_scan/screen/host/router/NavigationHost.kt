package com.dolgalyova.qr_scan.screen.host.router

import androidx.appcompat.app.AppCompatActivity

interface NavigationHost {
    var onAttachHost: () -> Unit
    fun attachHost(activity: AppCompatActivity)

    fun detachHost()

    fun getHost(): AppCompatActivity?
}