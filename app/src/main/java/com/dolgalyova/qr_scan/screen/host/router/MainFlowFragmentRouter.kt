package com.dolgalyova.qr_scan.screen.host.router

import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import com.dolgalyova.qr_scan.screen.main_flow.router.MainFlowRouter

class MainFlowFragmentRouter(private val navigator: AppNavigator) : MainFlowRouter {

    override fun openDetails(scan: ScanEntity) = navigator.setFlow(Flow.ScanDetails, scan)

    override fun goBack() = navigator.goBack()
}