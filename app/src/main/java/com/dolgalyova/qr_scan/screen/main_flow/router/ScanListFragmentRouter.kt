package com.dolgalyova.qr_scan.screen.main_flow.router

import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import com.dolgalyova.qr_scan.screen.scan_list.router.ScanListRouter

class ScanListFragmentRouter(private val navigator: MainFlowNavigator) : ScanListRouter {
    override fun openScanScreen() = navigator.setScreen(MainFlowNavigator.Screen.MakeScan)

    override fun openScan(scan: ScanEntity) = navigator.openScan(scan)
}