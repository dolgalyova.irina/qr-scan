package com.dolgalyova.qr_scan.screen.host

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.qr_scan.screen.host.router.AppNavigator
import com.dolgalyova.qr_scan.screen.host.router.Flow

class MainViewModel(navigator: AppNavigator) : ViewModel() {

    init {
        navigator.setFlow(Flow.Main)
    }
}

class MainViewModelFactory(private val navigator: AppNavigator) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MainViewModel(navigator) as T
}