package com.dolgalyova.qr_scan.screen.make_scan.domain

import com.dolgalyova.qr_scan.common.arch.Interactor
import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.util.composeWith
import com.dolgalyova.qr_scan.screen.make_scan.data.MakeScanRepository

class MakeScanInteractor(private val repository: MakeScanRepository,
                         private val workers: RxWorkers) : Interactor() {

    fun saveScanResult(result: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.saveResult(result)
                .composeWith(workers)
                .subscribe(onSuccess, onError))
    }
}