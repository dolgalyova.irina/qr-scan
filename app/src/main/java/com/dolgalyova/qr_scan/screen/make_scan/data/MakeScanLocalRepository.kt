package com.dolgalyova.qr_scan.screen.make_scan.data

import com.dolgalyova.qr_scan.common.data.database.ScanDao
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import com.dolgalyova.qr_scan.common.util.getOrNull
import com.dolgalyova.qr_scan.screen.make_scan.data.model.ReceiptQrData
import com.dolgalyova.qr_scan.screen.make_scan.source.ScanRemoteSource
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Single

class MakeScanLocalRepository(private val dao: ScanDao,
                              private val remoteSource: ScanRemoteSource,
                              private val gson: Gson) : MakeScanRepository {

    override fun saveResult(result: String): Completable {
        return Single
                .fromCallable {
                    val data = getOrNull { gson.fromJson<ReceiptQrData>(result, ReceiptQrData::class.java) }
                    data ?: throw IllegalArgumentException()
                }
                .doOnSuccess { receipt -> dao.insert(receipt.toScan()) }
                .flatMap {
                    remoteSource.getScanInfo(it.fiscalNumber, it.fiscalSign, it.fiscalDocument, it.date, it.sum)
                            .toSingleDefault(it)
                }
                .doOnSuccess {
                    val current = dao.getScanByFiscalSign(it.fiscalSign)
                    val updated = current.copy(isSent = true)
                    dao.update(updated)
                }
                .toCompletable()
    }
}

private fun ReceiptQrData.toScan(): ScanEntity {
    return ScanEntity(this.fiscalNumber, this.fiscalSign, this.fiscalDocument, this.date, this.sum, false)
}