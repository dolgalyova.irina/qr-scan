package com.dolgalyova.qr_scan.screen.scan_list.view

import androidx.recyclerview.widget.DiffUtil
import com.dolgalyova.qr_scan.screen.scan_list.presentation.ScanListItem

class ScanListItemDiffCallback : DiffUtil.ItemCallback<ScanListItem>() {

    override fun areItemsTheSame(oldItem: ScanListItem, newItem: ScanListItem): Boolean {
        return (oldItem is ScanListItem.Info && newItem is ScanListItem.Info && oldItem.item.fiscalSign == newItem.item.fiscalSign) ||
                (oldItem is ScanListItem.LoadNextProgress && newItem is ScanListItem.LoadNextProgress) ||
                (oldItem is ScanListItem.InitialProgress && newItem is ScanListItem.LoadNextProgress)
    }

    override fun areContentsTheSame(oldItem: ScanListItem, newItem: ScanListItem) = oldItem == newItem
}