package com.dolgalyova.qr_scan.screen.scan_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.qr_scan.BuildConfig
import com.dolgalyova.qr_scan.R
import com.dolgalyova.qr_scan.common.arch.OnBackPressListener
import com.dolgalyova.qr_scan.common.util.showGeneralError
import com.dolgalyova.qr_scan.screen.scan_details.di.ScanDetailsComponent
import com.dolgalyova.qr_scan.screen.scan_details.domain.ScanId
import com.dolgalyova.qr_scan.screen.scan_details.presentation.ScanDetailsViewModel
import com.dolgalyova.qr_scan.screen.scan_details.presentation.ScanDetailsViewModelFactory
import com.dolgalyova.qr_scan.screen.scan_details.presentation.ScanDetailsViewState
import kotlinx.android.synthetic.main.fragment_scan_details.*
import javax.inject.Inject
import com.dolgalyova.qr_scan.screen.scan_details.WebViewClient as ScanWebViewClient

class ScanDetailsFragment : Fragment(), OnBackPressListener {

    companion object {
        private const val EXTRA_SCAN_ID = "${BuildConfig.APPLICATION_ID}.EXTRA_SCAN_ID"

        fun create(scanId: Long): Fragment {
            val args = Bundle().apply { putLong(EXTRA_SCAN_ID, scanId) }
            return ScanDetailsFragment().apply { arguments = args }
        }
    }

    private val component by lazy {
        val scanId = ScanId(arguments?.getLong(EXTRA_SCAN_ID, -1) ?: -1)
        (activity as ScanDetailsComponent.ComponentProvider).provideScanDetailsComponent(scanId)
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScanDetailsViewModel::class.java)
    }

    @Inject lateinit var viewModelFactory: ScanDetailsViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_scan_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel.stateObservable.observe(viewLifecycleOwner, Observer(::renderState))
        viewModel.errorsObservable.observe(viewLifecycleOwner, Observer { showGeneralError() })
    }

    override fun onBackPress(): Boolean {
        viewModel.onBackClick()
        return true
    }

    private fun renderState(state: ScanDetailsViewState) = when (state) {
        is ScanDetailsViewState.Data -> showContent(state)
    }

    private fun showContent(content: ScanDetailsViewState.Data) {
        if (content.scan != webView.url) webView.loadUrl(content.scan)
        progress.isVisible = !content.isLoading
    }

    private fun initViews() {
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = ScanWebViewClient(viewModel::onUrlLoaded)
    }
}

private class WebViewClient(private val onUrlLoaded: (String) -> Unit) : WebViewClient() {

    override fun onPageCommitVisible(view: WebView?, url: String?) {
        super.onPageCommitVisible(view, url)
        onUrlLoaded(url.orEmpty())
    }
}