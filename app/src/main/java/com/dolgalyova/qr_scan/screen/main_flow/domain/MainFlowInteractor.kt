package com.dolgalyova.qr_scan.screen.main_flow.domain

import com.dolgalyova.qr_scan.common.arch.Interactor
import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.util.composeWith
import io.reactivex.Single
import timber.log.Timber

class MainFlowInteractor(private val workers: RxWorkers) : Interactor() {

    fun getFirstScreen(onSuccess: (MainFlowInitialScreen) -> Unit) {
        addSubscription(Single.fromCallable { MainFlowInitialScreen.Recent }
                .composeWith(workers)
                .subscribe(onSuccess, Timber::e))
    }
}