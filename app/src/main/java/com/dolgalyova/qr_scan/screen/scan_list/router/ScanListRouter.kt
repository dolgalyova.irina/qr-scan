package com.dolgalyova.qr_scan.screen.scan_list.router

import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity

interface ScanListRouter {

    fun openScan(scan: ScanEntity)
    fun openScanScreen()
}