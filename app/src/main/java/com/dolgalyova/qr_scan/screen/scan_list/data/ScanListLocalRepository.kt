package com.dolgalyova.qr_scan.screen.scan_list.data

import com.dolgalyova.qr_scan.common.data.database.ScanDao
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import io.reactivex.Single

class ScanListLocalRepository(private val dao: ScanDao) : ScanListRepository {

    override fun getScans(page: Int): Single<List<ScanEntity>> {
        return Single.fromCallable { dao.getScans() }
    }
}