package com.dolgalyova.qr_scan.screen.scan_details.data

import com.dolgalyova.qr_scan.common.data.database.ScanDao
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import io.reactivex.Single

class ScanDetailsLocalRepository(private val dao: ScanDao) : ScanDetailsRepository {

    override fun getScan(id: Long): Single<ScanEntity> {
        return Single.fromCallable { dao.getScanByFiscalSign(id) }
    }
}