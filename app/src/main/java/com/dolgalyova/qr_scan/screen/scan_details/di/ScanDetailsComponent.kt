package com.dolgalyova.qr_scan.screen.scan_details.di

import com.dolgalyova.qr_scan.common.di.FlowScope
import com.dolgalyova.qr_scan.screen.scan_details.ScanDetailsFragment
import com.dolgalyova.qr_scan.screen.scan_details.domain.ScanId
import dagger.BindsInstance
import dagger.Subcomponent

@FlowScope
@Subcomponent(modules = [ScanDetailsModule::class])
interface ScanDetailsComponent {

    fun inject(target: ScanDetailsFragment)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: ScanDetailsModule): Builder
        @BindsInstance fun scanId(@ScanIdQualifier scanId: Long): Builder

        fun build(): ScanDetailsComponent
    }

    interface ComponentProvider {
        fun provideScanDetailsComponent(id: ScanId): ScanDetailsComponent
    }
}