package com.dolgalyova.qr_scan.screen.scan_list.di


import com.dolgalyova.qr_scan.common.di.ScreenScope
import com.dolgalyova.qr_scan.screen.scan_list.ScanListFragment
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [ScanListModule::class])
interface ScanListComponent {

    fun inject(target: ScanListFragment)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: ScanListModule): Builder
        fun build(): ScanListComponent
    }

    interface ComponentProvider {
        fun provideScanListComponent(): ScanListComponent
    }
}