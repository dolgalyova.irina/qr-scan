package com.dolgalyova.qr_scan.screen.scan_details.presentation

sealed class ScanDetailsViewState {
    data class Data(val scan: String, val isLoading: Boolean) : ScanDetailsViewState()
    companion object {
        val EMPTY = Data("", true)
    }
}