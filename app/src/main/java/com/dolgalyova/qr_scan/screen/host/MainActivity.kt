package com.dolgalyova.qr_scan.screen.host

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.qr_scan.R
import com.dolgalyova.qr_scan.app.App
import com.dolgalyova.qr_scan.common.arch.OnBackPressListener
import com.dolgalyova.qr_scan.screen.host.di.DaggerMainComponent
import com.dolgalyova.qr_scan.screen.host.di.MainModule
import com.dolgalyova.qr_scan.screen.host.di.MainNavigationModule
import com.dolgalyova.qr_scan.screen.host.router.NavigationHost
import com.dolgalyova.qr_scan.screen.main_flow.MainFlowFragment
import com.dolgalyova.qr_scan.screen.main_flow.di.MainFlowComponent
import com.dolgalyova.qr_scan.screen.main_flow.di.MainFlowModule
import com.dolgalyova.qr_scan.screen.main_flow.di.MainFlowNavigationModule
import com.dolgalyova.qr_scan.screen.scan_details.di.ScanDetailsComponent
import com.dolgalyova.qr_scan.screen.scan_details.di.ScanDetailsModule
import com.dolgalyova.qr_scan.screen.scan_details.domain.ScanId
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainFlowComponent.ComponentProvider,
        ScanDetailsComponent.ComponentProvider {

    private val component by lazy {
        DaggerMainComponent.builder()
                .parent((application as App).component)
                .module(MainModule())
                .navigationModule(MainNavigationModule())
                .target(this)
                .build()
    }

    @Inject lateinit var viewModelFactory: MainViewModelFactory
    @Inject lateinit var navigationHost: NavigationHost

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        navigationHost.attachHost(this)
    }

    override fun onBackPressed() {
        val topFragment = supportFragmentManager.fragments.firstOrNull { it is OnBackPressListener }
        val processed = if (topFragment?.isVisible == true) {
            (topFragment as? OnBackPressListener)?.onBackPress() ?: false
        } else false
        if (!processed) super.onBackPressed()
    }

    override fun onStop() {
        super.onStop()
        navigationHost.detachHost()
    }

    override fun provideMainFlowComponent(target: MainFlowFragment): MainFlowComponent {
        return component.plusMainFlow()
                .module(MainFlowModule())
                .navigationModule(MainFlowNavigationModule())
                .target(target)
                .build()
    }

    override fun provideScanDetailsComponent(id: ScanId): ScanDetailsComponent {
        return component.plusScanDetailsFlow()
                .module(ScanDetailsModule())
                .scanId(id.id)
                .build()
    }
}