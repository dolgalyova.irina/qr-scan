package com.dolgalyova.qr_scan.screen.main_flow.di

import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.di.FlowScope
import com.dolgalyova.qr_scan.screen.main_flow.MainFlowFragment
import com.dolgalyova.qr_scan.screen.main_flow.MainFlowViewModelFactory
import com.dolgalyova.qr_scan.screen.main_flow.domain.MainFlowInteractor
import com.dolgalyova.qr_scan.screen.main_flow.router.MainFlowNavigator
import com.dolgalyova.qr_scan.screen.main_flow.router.MainFlowRouter
import dagger.Module
import dagger.Provides

@Module
class MainFlowModule {

    @Provides
    @FlowScope
    fun navigator(router: MainFlowRouter, host: MainFlowFragment) = MainFlowNavigator(host, router)

    @Provides
    @FlowScope
    fun interactor(workers: RxWorkers) = MainFlowInteractor(workers)

    @Provides
    @FlowScope
    fun mainFlowViewModelFactory(interactor: MainFlowInteractor, navigator: MainFlowNavigator) =
            MainFlowViewModelFactory(interactor, navigator)
}