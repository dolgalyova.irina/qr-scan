package com.dolgalyova.qr_scan.screen.main_flow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.qr_scan.screen.main_flow.domain.MainFlowInitialScreen
import com.dolgalyova.qr_scan.screen.main_flow.domain.MainFlowInitialScreen.Recent
import com.dolgalyova.qr_scan.screen.main_flow.domain.MainFlowInteractor
import com.dolgalyova.qr_scan.screen.main_flow.router.MainFlowNavigator
import com.dolgalyova.qr_scan.screen.main_flow.router.MainFlowNavigator.Screen.RecentScans

class MainFlowViewModel(private val interactor: MainFlowInteractor,
                        private val navigator: MainFlowNavigator) : ViewModel() {

    init {
        interactor.getFirstScreen(::onInitialScreenAvailable)
    }

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }

    private fun onInitialScreenAvailable(screen: MainFlowInitialScreen) = when (screen) {
        Recent -> navigator.setScreen(RecentScans)
    }
}

class MainFlowViewModelFactory(private val interactor: MainFlowInteractor,
                               private val navigator: MainFlowNavigator) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>) = MainFlowViewModel(interactor, navigator) as T
}