package com.dolgalyova.qr_scan.screen.main_flow.di

import com.dolgalyova.qr_scan.common.di.FlowScope
import com.dolgalyova.qr_scan.screen.main_flow.MainFlowFragment
import com.dolgalyova.qr_scan.screen.make_scan.di.MakeScanComponent
import com.dolgalyova.qr_scan.screen.scan_list.di.ScanListComponent
import dagger.BindsInstance
import dagger.Subcomponent

@FlowScope
@Subcomponent(modules = [MainFlowModule::class, MainFlowNavigationModule::class])
interface MainFlowComponent {

    fun inject(target: MainFlowFragment)

    fun plusScanList(): ScanListComponent.Builder
    fun plusMakeScan(): MakeScanComponent.Builder

    @Subcomponent.Builder
    interface Builder {
        fun module(module: MainFlowModule): Builder
        fun navigationModule(module: MainFlowNavigationModule): Builder
        @BindsInstance fun target(target: MainFlowFragment): Builder
        fun build(): MainFlowComponent
    }

    interface ComponentProvider {
        fun provideMainFlowComponent(target: MainFlowFragment): MainFlowComponent
    }
}