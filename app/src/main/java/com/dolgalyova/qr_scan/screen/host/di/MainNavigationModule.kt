package com.dolgalyova.qr_scan.screen.host.di

import com.dolgalyova.qr_scan.common.di.ActivityScope
import com.dolgalyova.qr_scan.screen.host.router.AppNavigator
import com.dolgalyova.qr_scan.screen.host.router.MainFlowFragmentRouter
import com.dolgalyova.qr_scan.screen.host.router.ScanDetailsFragmentRouter
import com.dolgalyova.qr_scan.screen.main_flow.router.MainFlowRouter
import com.dolgalyova.qr_scan.screen.scan_details.router.ScanDetailsRouter
import dagger.Module
import dagger.Provides

@Module
class MainNavigationModule {

    @Provides
    @ActivityScope
    fun mainFlowRouter(navigator: AppNavigator): MainFlowRouter = MainFlowFragmentRouter(navigator)

    @Provides
    @ActivityScope
    fun scanDetailsRouter(navigator: AppNavigator): ScanDetailsRouter = ScanDetailsFragmentRouter(navigator)
}