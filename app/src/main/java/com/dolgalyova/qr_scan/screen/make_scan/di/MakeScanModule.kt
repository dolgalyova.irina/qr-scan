package com.dolgalyova.qr_scan.screen.make_scan.di

import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.di.ScreenScope
import com.dolgalyova.qr_scan.common.data.database.AppDatabase
import com.dolgalyova.qr_scan.common.util.createApiService
import com.dolgalyova.qr_scan.screen.make_scan.data.MakeScanLocalRepository
import com.dolgalyova.qr_scan.screen.make_scan.data.MakeScanRepository
import com.dolgalyova.qr_scan.screen.make_scan.domain.MakeScanInteractor
import com.dolgalyova.qr_scan.screen.make_scan.presentation.MakeScanViewModelFactory
import com.dolgalyova.qr_scan.screen.make_scan.router.MakeScanRouter
import com.dolgalyova.qr_scan.screen.make_scan.source.ScanRemoteSource
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class MakeScanModule {

    @Provides
    @ScreenScope
    fun remoteSource(client: OkHttpClient) = createApiService(ScanRemoteSource::class.java, client)

    @Provides
    @ScreenScope
    fun repository(localSource: AppDatabase, remoteSource: ScanRemoteSource, gson: Gson): MakeScanRepository {
        return MakeScanLocalRepository(localSource.scan, remoteSource, gson)
    }

    @Provides
    @ScreenScope
    fun interactor(repository: MakeScanRepository, workers: RxWorkers) =
            MakeScanInteractor(repository, workers)

    @Provides
    @ScreenScope
    fun viewModelFactory(interactor: MakeScanInteractor, router: MakeScanRouter) =
            MakeScanViewModelFactory(interactor, router)
}