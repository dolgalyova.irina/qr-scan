package com.dolgalyova.qr_scan.screen.host.di

import com.dolgalyova.qr_scan.common.di.ActivityScope
import com.dolgalyova.qr_scan.screen.host.MainViewModelFactory
import com.dolgalyova.qr_scan.screen.host.router.AppNavigator
import com.dolgalyova.qr_scan.screen.host.router.NavigationHost
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @ActivityScope
    fun navigator(host: NavigationHost) = AppNavigator(host)

    @Provides
    @ActivityScope
    fun viewModelFactory(navigator: AppNavigator) = MainViewModelFactory(navigator)

    @Provides
    @ActivityScope
    fun gson() = GsonBuilder().create()
}