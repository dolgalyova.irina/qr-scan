package com.dolgalyova.qr_scan.screen.make_scan.di

import com.dolgalyova.qr_scan.common.di.ScreenScope
import com.dolgalyova.qr_scan.screen.make_scan.MakeScanFragment
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [MakeScanModule::class])
interface MakeScanComponent {

    fun inject(target: MakeScanFragment)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: MakeScanModule): Builder
        fun build(): MakeScanComponent
    }

    interface ComponentProvider {
        fun provideMakeScanComponent(): MakeScanComponent
    }
}