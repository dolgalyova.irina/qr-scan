package com.dolgalyova.qr_scan.screen.scan_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.qr_scan.R
import com.dolgalyova.qr_scan.common.util.setLoadMoreListener
import com.dolgalyova.qr_scan.common.util.showGeneralError
import com.dolgalyova.qr_scan.screen.scan_list.di.ScanListComponent
import com.dolgalyova.qr_scan.screen.scan_list.presentation.ScanListViewModel
import com.dolgalyova.qr_scan.screen.scan_list.presentation.ScanListViewModelFactory
import com.dolgalyova.qr_scan.screen.scan_list.view.ScanListAdapter
import kotlinx.android.synthetic.main.fragment_scan_list.*
import javax.inject.Inject

class ScanListFragment : Fragment() {

    companion object {
        fun create(): ScanListFragment {
            return ScanListFragment()
        }
    }

    private val component by lazy {
        (parentFragment as ScanListComponent.ComponentProvider).provideScanListComponent()
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScanListViewModel::class.java)
    }

    @Inject lateinit var viewModelFactory: ScanListViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_scan_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()

        viewModel.itemsObservable.observe(viewLifecycleOwner,
                Observer { (scanList.adapter as ScanListAdapter).submitList(it) })
        viewModel.generalErrorObservable.observe(viewLifecycleOwner, Observer { showGeneralError() })
    }

    private fun initViews() {
        scanList.layoutManager = LinearLayoutManager(context)
                .apply { orientation = RecyclerView.VERTICAL }
        scanList.itemAnimator = DefaultItemAnimator()
        scanList.adapter = ScanListAdapter(viewModel::onScanClick)
        scanList.setLoadMoreListener(viewModel::onLoadMore)

        addScan.setOnClickListener { viewModel.onAddScanClick() }
    }
}