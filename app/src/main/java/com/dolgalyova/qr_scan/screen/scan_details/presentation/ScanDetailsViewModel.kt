package com.dolgalyova.qr_scan.screen.scan_details.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dolgalyova.qr_scan.common.arch.SingleLiveEvent
import com.dolgalyova.qr_scan.screen.scan_details.domain.ScanDetailsInteractor
import com.dolgalyova.qr_scan.screen.scan_details.router.ScanDetailsRouter
import timber.log.Timber

class ScanDetailsViewModel(private val interactor: ScanDetailsInteractor,
                           private val router: ScanDetailsRouter) : ViewModel() {
    private val state = MutableLiveData<ScanDetailsViewState>()
    val stateObservable: LiveData<ScanDetailsViewState>
        get() = state
    private val errors = SingleLiveEvent<Any>()
    val errorsObservable: LiveData<Any>
        get() = errors

    init {
        state.value = ScanDetailsViewState.EMPTY
        interactor.getScan(::onDataAvailable, ::onError)
    }

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }

    fun onBackClick() = router.exit()

    fun onUrlLoaded(url: String) {
        state.value = currentData().copy(isLoading = url.startsWith("http", true))
    }

    private fun onDataAvailable(scan: String) {
        state.value = currentData().copy(scan = scan)
    }

    private fun onError(error: Throwable) {
        Timber.e(error)
        errors.call()
    }

    private fun currentData() = state.value as? ScanDetailsViewState.Data
            ?: ScanDetailsViewState.EMPTY
}