package com.dolgalyova.qr_scan.screen.main_flow.di

import com.dolgalyova.qr_scan.common.di.FlowScope
import com.dolgalyova.qr_scan.screen.main_flow.router.MainFlowNavigator
import com.dolgalyova.qr_scan.screen.main_flow.router.MakeScanFragmentRouter
import com.dolgalyova.qr_scan.screen.main_flow.router.ScanListFragmentRouter
import com.dolgalyova.qr_scan.screen.make_scan.router.MakeScanRouter
import com.dolgalyova.qr_scan.screen.scan_list.router.ScanListRouter
import dagger.Module
import dagger.Provides

@Module
class MainFlowNavigationModule {

    @Provides
    @FlowScope
    fun scanListRouter(navigator: MainFlowNavigator): ScanListRouter =
            ScanListFragmentRouter(navigator)

    @Provides
    @FlowScope
    fun makeScanRouter(navigator: MainFlowNavigator): MakeScanRouter =
            MakeScanFragmentRouter(navigator)
}