package com.dolgalyova.qr_scan.screen.scan_list.di

import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.di.ScreenScope
import com.dolgalyova.qr_scan.common.data.database.AppDatabase
import com.dolgalyova.qr_scan.screen.scan_list.data.ScanListLocalRepository
import com.dolgalyova.qr_scan.screen.scan_list.data.ScanListRepository
import com.dolgalyova.qr_scan.screen.scan_list.domain.ScanListInteractor
import com.dolgalyova.qr_scan.screen.scan_list.presentation.ScanListViewModelFactory
import com.dolgalyova.qr_scan.screen.scan_list.router.ScanListRouter
import dagger.Module
import dagger.Provides

@Module
class ScanListModule {

    @Provides
    @ScreenScope
    fun repository(localSource: AppDatabase): ScanListRepository =
            ScanListLocalRepository(localSource.scan)

    @Provides
    @ScreenScope
    fun interactor(repository: ScanListRepository, workers: RxWorkers) =
            ScanListInteractor(repository, workers)

    @Provides
    @ScreenScope
    fun viewModelFactory(interactor: ScanListInteractor, router: ScanListRouter) =
            ScanListViewModelFactory(interactor, router)
}