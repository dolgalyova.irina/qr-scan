package com.dolgalyova.qr_scan.screen.main_flow.router

import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import com.dolgalyova.qr_scan.screen.scan_list.data.model.ScanInfo

interface MainFlowRouter {

    fun openDetails(scan: ScanEntity)

    fun goBack()
}