package com.dolgalyova.qr_scan.screen.scan_list.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.qr_scan.R
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import com.dolgalyova.qr_scan.common.util.getString
import com.dolgalyova.qr_scan.screen.scan_list.presentation.ScanListItem
import kotlinx.android.synthetic.main.item_scan_list_scan.view.*

class ScanListAdapter(private val onItemClick: (ScanEntity) -> Unit) :
        ListAdapter<ScanListItem, RecyclerView.ViewHolder>(ScanListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_scan_list_scan -> ScanHolder(itemView, onItemClick)
            else -> ProgressHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        if (item is ScanListItem.Info) {
            val scan = item.item
            (holder as? ScanHolder)?.scan = scan
            with(holder.itemView) {
                scanDate.text = getString(R.string.label_date_template, scan.date)
                fiscalDocument.text = getString(R.string.label_document_template, scan.fiscalDocument)
                fiscalNum.text = getString(R.string.label_num_template, scan.fiscalNumber)
                fiscalSign.text = getString(R.string.label_sign_template, scan.fiscalSign)
                scanSum.text = getString(R.string.label_sum_template, scan.sum)
                isUpload.isVisible = scan.isSent
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return when (item) {
            is ScanListItem.Info -> R.layout.item_scan_list_scan
            is ScanListItem.InitialProgress -> R.layout.item_scan_list_initial_progress
            is ScanListItem.LoadNextProgress -> R.layout.item_scan_list_load_more_progress
        }
    }

    class ScanHolder(itemView: View, onItemClick: (ScanEntity) -> Unit) : RecyclerView.ViewHolder(itemView) {
        internal var scan: ScanEntity? = null

        init {
            itemView.setOnClickListener { _ -> scan?.let { onItemClick(it) } }
        }
    }

    class ProgressHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}