package com.dolgalyova.qr_scan.screen.make_scan

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.qr_scan.R
import com.dolgalyova.qr_scan.common.arch.OnBackPressListener
import com.dolgalyova.qr_scan.common.util.hasCameraPermission
import com.dolgalyova.qr_scan.screen.make_scan.di.MakeScanComponent
import com.dolgalyova.qr_scan.screen.make_scan.presentation.MakeScanViewModel
import com.dolgalyova.qr_scan.screen.make_scan.presentation.MakeScanViewModelFactory
import kotlinx.android.synthetic.main.fragment_make_scan.*
import javax.inject.Inject

class MakeScanFragment : Fragment(), OnBackPressListener {

    companion object {
        private const val CAMERA_REQUEST_CODE = 1004

        fun create(): MakeScanFragment = MakeScanFragment()
    }

    private val component by lazy {
        (parentFragment as MakeScanComponent.ComponentProvider).provideMakeScanComponent()
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MakeScanViewModel::class.java)
    }

    @Inject lateinit var viewModelFactory: MakeScanViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_make_scan, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.observeGeneralErrors.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), R.string.error_general, Toast.LENGTH_SHORT).show()
        })
        checkCameraPermission()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        scannerView.stopCamera()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (CAMERA_REQUEST_CODE == requestCode) {
            if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
                checkCameraPermission()
            } else {
                context?.let {
                    Toast.makeText(it, getString(R.string.error_permissions), Toast.LENGTH_SHORT).show()
                    viewModel.onBackClick()
                }
            }
        }
    }

    override fun onBackPress(): Boolean {
        viewModel.onBackClick()
        return true
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun checkCameraPermission() {
        if (hasCameraPermission()) {
            initViews()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
        }
    }

    private fun initViews() {
        scannerView.setResultHandler {
            scannerView.stopCamera()
            showProgress()
            viewModel.onScanResultAvailable(it.text.orEmpty())
        }
        scannerView.startCamera()
    }
}