package com.dolgalyova.qr_scan.screen.host.router

import com.dolgalyova.qr_scan.screen.scan_details.router.ScanDetailsRouter

class ScanDetailsFragmentRouter(private val navigator: AppNavigator) : ScanDetailsRouter {

    override fun exit() = navigator.goBack()
}