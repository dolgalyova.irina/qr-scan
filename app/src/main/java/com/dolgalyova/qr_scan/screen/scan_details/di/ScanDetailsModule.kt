package com.dolgalyova.qr_scan.screen.scan_details.di

import com.dolgalyova.qr_scan.common.arch.RxWorkers
import com.dolgalyova.qr_scan.common.di.FlowScope
import com.dolgalyova.qr_scan.common.data.database.AppDatabase
import com.dolgalyova.qr_scan.screen.scan_details.data.ScanDetailsLocalRepository
import com.dolgalyova.qr_scan.screen.scan_details.data.ScanDetailsRepository
import com.dolgalyova.qr_scan.screen.scan_details.domain.ScanDetailsInteractor
import com.dolgalyova.qr_scan.screen.scan_details.domain.ScanId
import com.dolgalyova.qr_scan.screen.scan_details.presentation.ScanDetailsViewModelFactory
import com.dolgalyova.qr_scan.screen.scan_details.router.ScanDetailsRouter
import dagger.Module
import dagger.Provides

@Module
class ScanDetailsModule {

    @Provides
    @FlowScope
    fun repository(localSource: AppDatabase): ScanDetailsRepository =
            ScanDetailsLocalRepository(localSource.scan)

    @Provides
    @FlowScope
    fun interactor(repository: ScanDetailsRepository,
                   @ScanIdQualifier scanId: Long, workers: RxWorkers) =
            ScanDetailsInteractor(repository, ScanId(scanId), workers)

    @Provides
    @FlowScope
    fun viewModelFactory(interactor: ScanDetailsInteractor, router: ScanDetailsRouter) =
            ScanDetailsViewModelFactory(interactor, router)
}