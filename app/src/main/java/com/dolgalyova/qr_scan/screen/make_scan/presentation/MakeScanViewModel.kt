package com.dolgalyova.qr_scan.screen.make_scan.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.qr_scan.common.arch.SingleLiveEvent
import com.dolgalyova.qr_scan.screen.make_scan.domain.MakeScanInteractor
import com.dolgalyova.qr_scan.screen.make_scan.router.MakeScanRouter
import timber.log.Timber

class MakeScanViewModel(private val interactor: MakeScanInteractor,
                        private val router: MakeScanRouter) : ViewModel() {
    private val generalError = SingleLiveEvent<Any>()
    val observeGeneralErrors: LiveData<Any>
        get() = generalError

    fun onScanResultAvailable(it: String) {
        interactor.saveScanResult(it, { router.exit() }, ::onError)
    }

    fun onBackClick() = router.exit()

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }

    private fun onError(error: Throwable) {
        Timber.e(error)
        generalError.call()
        router.exit()
    }
}

class MakeScanViewModelFactory(private val interactor: MakeScanInteractor,
                               private val router: MakeScanRouter) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MakeScanViewModel(interactor, router) as T
}