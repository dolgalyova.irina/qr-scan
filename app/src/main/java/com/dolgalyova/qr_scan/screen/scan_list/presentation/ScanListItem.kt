package com.dolgalyova.qr_scan.screen.scan_list.presentation

import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity

sealed class ScanListItem {
    object InitialProgress : ScanListItem()
    object LoadNextProgress : ScanListItem()
    class Info(val item: ScanEntity) : ScanListItem()
}