package com.dolgalyova.qr_scan.screen.scan_details.data

import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import io.reactivex.Single

interface ScanDetailsRepository {
    fun getScan(id: Long): Single<ScanEntity>
}