package com.dolgalyova.qr_scan.screen.scan_details.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ScanIdQualifier