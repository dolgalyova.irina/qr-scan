package com.dolgalyova.qr_scan.screen.scan_list.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.qr_scan.common.arch.SingleLiveEvent
import com.dolgalyova.qr_scan.common.data.database.entity.ScanEntity
import com.dolgalyova.qr_scan.screen.scan_list.domain.ScanListInteractor
import com.dolgalyova.qr_scan.screen.scan_list.router.ScanListRouter
import timber.log.Timber

class ScanListViewModel(private val interactor: ScanListInteractor,
                        private val router: ScanListRouter) : ViewModel() {
    private var isLoading = false
        set(value) {
            field = value
            val currentItems = items.value.orEmpty()
            if (field) {
                val loading = if (currentItems.isEmpty()) ScanListItem.InitialProgress
                else ScanListItem.LoadNextProgress

                items.value = currentItems + loading
            } else {
                items.value = currentItems.filter { it is ScanListItem.Info }
            }
        }
    private var page: Int = 0
    private val items = MutableLiveData<List<ScanListItem>>()
    val itemsObservable: LiveData<List<ScanListItem>>
        get() = items
    private val errors = SingleLiveEvent<Any>()
    val generalErrorObservable: LiveData<Any>
        get() = errors

    init {
        loadNextPage()
    }

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }

    fun onScanClick(scan: ScanEntity) = router.openScan(scan)

    fun onAddScanClick() = router.openScanScreen()

    fun onLoadMore() = loadNextPage()

    private fun loadNextPage() {
        if (isLoading) return
        isLoading = true
        page += 1
        interactor.getScans(page, ::onPageAvailable, ::onError)
    }

    private fun onPageAvailable(page: List<ScanEntity>) {
        val currentItems = items.value.orEmpty()
        val updatedScans = (currentItems + page.map { ScanListItem.Info(it) })
                .filterIsInstance(ScanListItem.Info::class.java)
                .distinctBy { it.item.fiscalSign }
        items.value = updatedScans
        isLoading = false
    }

    private fun onError(error: Throwable) {
        page -= 1
        isLoading = false
        Timber.e(error)
        errors.call()
    }
}

class ScanListViewModelFactory(private val interactor: ScanListInteractor,
                               private val router: ScanListRouter) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = ScanListViewModel(interactor, router) as T
}